<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->nullable()->after('name');
            $table->string('firstName')->nullable()->after('username');
            $table->string('lastName')->nullable()->after('firstName');
            $table->dateTime('birthDate')->nullable()->after('email_verified_at');
            $table->double('basicSalary')->nullable()->after('birthDate');
            $table->string('status')->nullable()->after('basicSalary');
            $table->string('group')->nullable()->after('status');
            $table->string('description')->nullable()->after('group');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'username',
                'firstName',
                'lastName',
                'birthDate',
                'basicSalary',
                'status',
                'group',
                'description',
            ]);
        });
    }
};
