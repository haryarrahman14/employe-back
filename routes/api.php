<?php

use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [UserController::class, 'logout']);
    
    /** SINGULAR */
    Route::get('user', [UserController::class, 'fetch']);
    Route::post('user', [UserController::class, 'updateProfile']);
    
    /** PLURAL */
    Route::get('users', [UserController::class, 'getAllUsers']);
    Route::post('users', [UserController::class, 'createUser']);
    Route::get('users/{id}', [UserController::class, 'getUserById']);
    Route::put('users/{id}', [UserController::class, 'updateUserById']);
    Route::delete('users/{id}', [UserController::class, 'deleteUserById']);

});