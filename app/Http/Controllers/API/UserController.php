<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password as RulesPassword;

class UserController extends Controller
{
    public function register(Request $request)
    {
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', new RulesPassword(8)],
            ]);

            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $user = User::where('email', $request->email)->first();
            $token = $user->createToken($request->email)->plainTextToken;

            return ResponseFormatter::success([
                'access_token' => $token,
                'token_type' => 'Bearer',
                'user' => $user,
            ], 'User registered successfully');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Registration failed', 500);
        }
    }

    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => ['required', 'email'],
                'password' => ['required'],
            ]);

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials)) {
                return ResponseFormatter::error([
                    'message' => 'Unauthorized',
                ], 'Login failed', 500);
            }

            $user = User::where('email', $request->email)->first();

            if (!Hash::check($request->password, $user->password, [])) {
                throw new \Exception('Invalid credentials');
            }

            $token = $user->createToken($request->email)->plainTextToken;

            return ResponseFormatter::success([
                'access_token' => $token,
                'token_type' => 'Bearer',
                'user' => $user,
            ], 'User logged in successfully');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Login failed', 500);
        }
    }

    public function fetch(Request $request)
    {
        return ResponseFormatter::success($request->user(), 'User profile retrieved successfully');
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $user->update($data);

        return ResponseFormatter::success($user, 'Profile updated successfully');
    }

    public function logout(Request $request)
    {
        $user = Auth::user();
        $user->currentAccessToken()->delete();

        return ResponseFormatter::success([], 'Token revoked successfully');
    }

    public function getAllUsers(Request $request)
    {
       
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 6);
        $name = $request->input('name');
        $sort = $request->input('sort', 'name');
        $order = $request->input('order', 'asc');

        $query = User::query();

        if ($name) {
            $query->where('name', 'LIKE', "%$name%");
        }

        $users = $query->orderBy($sort, $order)
                       ->paginate($limit, ['*'], 'page', $page);

        return ResponseFormatter::success($users, 'All users retrieved successfully');
    }

    public function getUserById($id)
    {
        $user = User::find($id);
        if (!$user) {
            return ResponseFormatter::error(null, 'User not found', 404);
        }
        return ResponseFormatter::success($user, 'User retrieved successfully');
    }

    public function createUser(Request $request)
    {
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', new RulesPassword(8)],
                'firstName' => ['required', 'string', 'max:255'],
                'lastName' => ['required', 'string', 'max:255'],
                'birthDate' => ['required', 'date'],
                'basicSalary' => ['required', 'numeric'],
                'status' => ['required', 'string', 'max:255'],
                'group' => ['required', 'string', 'max:255'],
                'description' => ['required', 'string'],
            ]);

            $user = User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'birthDate' => $request->birthDate,
                'basicSalary' => $request->basicSalary,
                'status' => $request->status,
                'group' => $request->group,
                'description' => $request->description,
            ]);

            return ResponseFormatter::success($user, 'User created successfully');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'User creation failed', 500);
        }
    }

    public function updateUserById(Request $request, $id)
    {
        try {
            $user = User::find($id);
            if (!$user) {
                return ResponseFormatter::error(null, 'User not found', 404);
            }

            $request->validate([
                'name' => ['sometimes', 'string', 'max:255'],
                'username' => ['sometimes', 'string', 'max:255', 'unique:users,username,' . $id],
                'email' => ['sometimes', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
                'password' => ['sometimes', 'string', new RulesPassword(8)],
                'firstName' => ['nullable', 'string', 'max:255'],
                'lastName' => ['nullable', 'string', 'max:255'],
                'birthDate' => ['nullable', 'date'],
                'basicSalary' => ['nullable', 'numeric'],
                'status' => ['nullable', 'string', 'max:255'],
                'group' => ['nullable', 'string', 'max:255'],
                'description' => ['nullable', 'string'],
            ]);

            $data = $request->all();
            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            }
            $user->update($data);

            return ResponseFormatter::success($user, 'User updated successfully');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'User update failed', 500);
        }
    }

    public function deleteUserById($id)
    {
        try {
            $user = User::find($id);
            if (!$user) {
                return ResponseFormatter::error(null, 'User not found', 404);
            }
            $user->delete();
            return ResponseFormatter::success(null, 'User deleted successfully');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'User deletion failed', 500);
        }
    }
}
